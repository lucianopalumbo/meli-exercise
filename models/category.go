package models

// A MELI category node. It has its information
// and the child categories that it has.
type Category struct {
	ID                 string     `json:"id"`
	Name               string     `json:"name"`
	TotalItems         int        `json:"total_items_in_this_category"`
	ChildrenCategories []Category `json:"children_categories"`
}

// Checks if this category is a leaf node (that it doesn't have childs).
func (c *Category) IsLeaf() bool {
	return len(c.ChildrenCategories) == 0
}
