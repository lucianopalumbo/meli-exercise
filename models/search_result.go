package models

// MELI search method result.
type SearchResult struct {
	Paging  PageInfo     `json:"paging"`
	Results []SearchItem `json:"results"`
}

// Pagination info of search method.
type PageInfo struct {
	Total  int `json:"total"`
	Offset int `json:"offset"`
	Limit  int `json:"limit"`
}

// Price of a determined item.
type SearchItem struct {
	Id    string  `json:"id"`
	Price float32 `json:"price"`
}
