package responses

// Generic response for all methods.
type Response struct {
	ErrorMessage string `json:"error,omitempty"`
}
