package responses

// "Prices" method response.
type PricesResponse struct {
	Response
	MinPrice       float32 `json:"min"`
	MaxPrice       float32 `json:"max"`
	SuggestedPrice float32 `json:"suggested"`
}
