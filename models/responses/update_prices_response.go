package responses

// "UpdatePrices" method response.
type UpdatePricesResponse struct {
	Response
	Message string `json:"message, omitempty"`
}
