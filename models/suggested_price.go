package models

// The price information for a category item.
type SuggestedPrice struct {
	CategoryID     string  `json:"category"`
	MinPrice       float32 `json:"min"`
	MaxPrice       float32 `json:"max"`
	SuggestedPrice float32 `json:"suggested"`
}
