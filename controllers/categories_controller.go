package controllers

import (
	"log"
	"net/http"

	"meli-exercise/repositories"
	"meli-exercise/services/categories"
	"meli-exercise/services/products"

	"github.com/gin-gonic/gin"
)

type CategoriesController struct {
	Service services.CategoriesService
}

// Obtains the minimum, maximum and suggested item price
// for a category of items.
func (c *CategoriesController) Prices(context *gin.Context) {
	id := context.Param("id")

	if response, err := c.Service.Prices(id); err != nil {
		if _, ok := err.(*repositories.ItemNotFoundError); ok {
			context.JSON(http.StatusNotFound, response)
		} else {
			log.Printf("Error while fetching suggested price for category %s - %s\n", id, err.Error())
			context.JSON(http.StatusInternalServerError, response)
		}
	} else {
		context.JSON(http.StatusOK, response)
	}
}

// Calculates the minimum, maximum and suggested item price
// for a category of items. This method instantly returns
// and runs as a background process.
func (c *CategoriesController) UpdatePrice(context *gin.Context) {
	id := context.Param("id")

	if response, err := c.Service.UpdatePrice(id); err != nil {
		if _, ok := err.(*services_products.ItemNotFoundError); ok {
			context.JSON(http.StatusNotFound, response)
		} else {
			log.Printf("Error while updating suggested price for category %s - %s\n", id, err.Error())
			context.JSON(http.StatusInternalServerError, response)
		}
	} else {
		context.JSON(http.StatusOK, response)
	}
}

// Calculates the minimum, maximum and suggested item price
// for all categories of items. This method instantly returns
// and runs as a background process.
func (c *CategoriesController) UpdatePrices(context *gin.Context) {
	if response, err := c.Service.UpdatePrices(); err != nil {
		log.Printf("Error while updating suggested prices for all categories %s\n", err.Error())
		context.JSON(http.StatusInternalServerError, response)
	} else {
		context.JSON(http.StatusOK, response)
	}
}
