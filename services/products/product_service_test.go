package services_products

import (
	"errors"
	"meli-exercise/models"
	"reflect"
	"testing"
)

const FAKE_CATEGORY string = "FAKE123"
const GUITAR_CATEGORY string = "MLA12497"

// This mock API returns the same objets that
// it receives when it's built
type MockAPI struct {
	Response interface{}
	Error    error
}

func (m *MockAPI) Get(url string) (interface{}, error) {
	return m.Response, m.Error
}

// A unit test for getting a product
func TestSearch(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	expectedSearchResult := models.SearchResult{
		Results: []models.SearchItem{
			models.SearchItem{
				Id:    "PRODUCT1",
				Price: 2000,
			},
			models.SearchItem{
				Id:    "PRODUCT2",
				Price: 2500,
			},
		},
	}

	service.Api = &MockAPI{
		Response: expectedSearchResult,
	}

	searchResult, err := service.Search([]Argument{
		Argument{CATEGORY, FAKE_CATEGORY},
	})

	if err != nil {
		t.Fatalf("There was an error in TestSearch: %s", err)
	}

	if !reflect.DeepEqual(searchResult.Results, expectedSearchResult.Results) {
		t.Fatalf("Expected %+v but got %+v", expectedSearchResult, searchResult)
	}
}

// A unit test for searching an inexistent product
func TestSearchNotFound(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	service.Api = &MockAPI{
		Error: &ItemNotFoundError{ID: FAKE_CATEGORY},
	}

	_, err := service.Search([]Argument{
		Argument{CATEGORY, FAKE_CATEGORY},
	})

	if err == nil {
		t.Fatal("Expected an error in TestSearch, but there's none")
	}
}

// A unit test for forcing an error while searching a product
func TestSearchError(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	service.Api = &MockAPI{
		Error: errors.New("This is a test error"),
	}

	_, err := service.Search([]Argument{
		Argument{CATEGORY, FAKE_CATEGORY},
	})

	if err == nil {
		t.Fatal("Expected an error in TestSearch, but there's none")
	}
}

// A unit test for getting a category
func TestCategory(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	expectedCategory := models.Category{
		ID: FAKE_CATEGORY,
	}

	service.Api = &MockAPI{
		Response: expectedCategory,
	}

	category, err := service.Category(FAKE_CATEGORY)

	if err != nil {
		t.Fatalf("There was an error in TestCategory: %s", err)
	}

	if !reflect.DeepEqual(category, expectedCategory) {
		t.Fatalf("Expected %+v but got %+v", expectedCategory, category)
	}
}

// A unit test for searching an inexistent category
func TestCategoryNotFound(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	service.Api = &MockAPI{
		Error: &ItemNotFoundError{ID: FAKE_CATEGORY},
	}

	_, err := service.Category(FAKE_CATEGORY)

	if err == nil {
		t.Fatal("Expected an error in TestCategory, but there's none")
	}
}

// A unit test for forcing an error while searching a product
func TestCategoryError(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	service.Api = &MockAPI{
		Error: errors.New("This is a test error"),
	}

	_, err := service.Category(FAKE_CATEGORY)

	if err == nil {
		t.Fatal("Expected an error in TestCategory, but there's none")
	}
}

// A unit test for getting all available categories
func TestAllRootCategories(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	expectedCategories := []models.Category{
		models.Category{ID: FAKE_CATEGORY},
		models.Category{ID: FAKE_CATEGORY + "_1"},
	}

	service.Api = &MockAPI{
		Response: expectedCategories,
	}

	categories, err := service.AllRootCategories()

	if err != nil {
		t.Fatalf("There was an error in TestAllRootCategories: %s", err)
	}

	if !reflect.DeepEqual(categories, expectedCategories) {
		t.Fatalf("Expected %+v but got %+v", expectedCategories, categories)
	}
}

// A real test that queries MercadoLibre's API and gets
// information about Gibson guitars
func TestMeliCategory(t *testing.T) {
	service := MeliProductsService{}
	service.Init()

	category, err := service.Category(GUITAR_CATEGORY)

	expectedCategory := models.Category{
		ID:   GUITAR_CATEGORY,
		Name: "Gibson",
	}

	if err != nil {
		t.Fatalf("There was an error in TestMeliCategory: %s", err)
	}

	// For simplicity, compare only these two fields (items may vary)
	if category.ID != expectedCategory.ID || category.Name != expectedCategory.Name {
		t.Fatalf("Expected %+v but got %+v", expectedCategory, category)
	}
}
