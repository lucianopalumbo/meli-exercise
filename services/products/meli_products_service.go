package services_products

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"meli-exercise/models"

	"github.com/mercadolibre/golang-restclient/rest"
)

const SEARCH_METHOD string = "https://api.mercadolibre.com/sites/MLA/search"
const LIST_ALL_ROOT_CATEGORIES string = "https://api.mercadolibre.com/sites/MLA/categories"
const CATEGORY_METHOD string = "https://api.mercadolibre.com/categories"

// Time that must be waited for redoing another request.
const SLEEP_TIME time.Duration = 500 * time.Millisecond

var mutex *sync.Mutex

type MeliAPI struct{}

func (m *MeliAPI) Get(url string) (interface{}, error) {
	var response interface{}
	var err error

	// Since public MELI's API blocks DDoS, a little time must
	// be waited for each request.
	mutex.Lock()

	// Uses MELI's Golang REST Client for querying the information.
	resp := rest.Get(url)
	time.Sleep(SLEEP_TIME)

	mutex.Unlock()

	if resp.Response == nil {
		err = errors.New(fmt.Sprintf("Error getting info from %s", url))
	} else if resp.Response.StatusCode == http.StatusNotFound {
		err = &ItemNotFoundError{}
	} else if resp.Response.StatusCode == http.StatusOK {
		err = json.Unmarshal(resp.Bytes(), &response)
	} else {
		err = errors.New(fmt.Sprintf("Error getting info from %s", url))
	}

	return response, err
}

// Search API could be mocked, for testing purposes.
type API interface {
	Get(url string) (interface{}, error)
}

// Implementation of the products' service of MercadoLibre.
type MeliProductsService struct {
	Api API
}

// Initializes the service. Must be called first before using it.
func (m *MeliProductsService) Init() {
	m.Api = &MeliAPI{}
	mutex = &sync.Mutex{}
}

// Performs an item search, returning basic item information.
func (m *MeliProductsService) Search(params []Argument) (models.SearchResult, error) {
	var searchResult models.SearchResult
	url := urlBuilder(SEARCH_METHOD, params)

	resp, err := m.Api.Get(url)
	if err != nil {
		err = errors.New(fmt.Sprintf("There was an error in MELI Search call: %s", err))
	} else {
		data, _ := json.Marshal(resp)
		err = json.Unmarshal(data, &searchResult)

		if err != nil {
			err = errors.New(fmt.Sprintf("There was an error in MELI Search call: %s", err))
		}
	}

	return searchResult, err
}

// Gets all information of a category by its ID.
func (m *MeliProductsService) Category(categoryID string) (models.Category, error) {
	var category models.Category

	url := fmt.Sprintf("%s/%s", CATEGORY_METHOD, categoryID)
	resp, err := m.Api.Get(url)

	if err != nil {
		if _, ok := err.(*ItemNotFoundError); ok {
			err.(*ItemNotFoundError).ID = categoryID
		} else {
			err = errors.New(fmt.Sprintf("There was an error in MELI Category call: %s", err))
		}
	} else {
		data, _ := json.Marshal(resp)
		err = json.Unmarshal(data, &category)

		if err != nil {
			err = errors.New(fmt.Sprintf("There was an error in MELI Category call: %s", err))
		}
	}

	return category, err
}

// Gets all root product categories.
func (m *MeliProductsService) AllRootCategories() ([]models.Category, error) {
	var categories []models.Category
	resp, err := m.Api.Get(LIST_ALL_ROOT_CATEGORIES)

	if err != nil {
		err = errors.New(fmt.Sprintf("There was an error in MELI AllRootCategories call: %s", err))
	} else {
		data, _ := json.Marshal(resp)
		err = json.Unmarshal(data, &categories)

		if err != nil {
			err = errors.New(fmt.Sprintf("There was an error in MELI AllRootCategories call: %s", err))
		}
	}

	return categories, err
}

// Helper function for building a endpoint by a parameter list.
func urlBuilder(url string, params []Argument) string {
	if len(params) > 0 {
		url = url + "?"

		for _, v := range params {
			url += fmt.Sprintf("%s=%s&", v.Parameter, v.Value)
		}

		url = strings.TrimSuffix(url, "&")
	}

	return url
}
