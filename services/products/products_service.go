package services_products

import (
	"fmt"
	"meli-exercise/models"
)

const CATEGORY string = "category"
const OFFSET string = "offset"
const SORT string = "sort"
const SORT_RELEVANCE string = "relevance"
const CONDITION string = "condition"
const CONDITION_NEW string = "new"
const BUYING_MODE string = "buying_mode"
const BUYING_MODE_BUY_IT_NOW string = "buy_it_now"
const POWER_SELLER string = "power_seller"
const ADULT_CONTENT string = "adult_content"
const YES string = "yes"

// Search method filtering.
type Argument struct {
	Parameter string
	Value     string
}

// Abstraction used to obtain information of the products of a e-commerce.
type ProductsService interface {
	// Initializes the service. Must be called first before using it.
	Init()

	// Performs an item search, returning basic item information.
	Search(params []Argument) (models.SearchResult, error)

	// Gets all information of a category by its ID.
	Category(categoryID string) (models.Category, error)

	// Gets all root product categories.
	AllRootCategories() ([]models.Category, error)
}

// Error that is thrown when an item is not found.
type ItemNotFoundError struct {
	ID string
}

// Gets the error message as a string.
func (e *ItemNotFoundError) Error() string {
	return fmt.Sprintf("%s not found", e.ID)
}
