package services

import (
	"errors"
	"fmt"
	"log"
	"sort"
	"strconv"
	"sync"

	"meli-exercise/models"
	"meli-exercise/models/responses"
	"meli-exercise/repositories"
	"meli-exercise/services/products"
)

// Concrete categories' service implementation.
type MELICategoriesService struct {
	Repository repositories.CategoriesRepository
	Products   services_products.ProductsService
}

// Maximum number of items that will be queried for a category.
const QUERY_MAX_ITEMS = 100

// Maximum number of worker threads that will read the shared
// category id channel
const LEAF_CHANNEL_THREADS_COUNT = 10

// Obtains the minimum, maximum and suggested item price
// for a category of items.
func (s *MELICategoriesService) Prices(id string) (responses.PricesResponse, error) {
	var response responses.PricesResponse
	suggestedPrice, err := s.Repository.Get(id)

	if err != nil {
		if _, ok := err.(*repositories.ItemNotFoundError); ok {
			response = responses.PricesResponse{
				Response: responses.Response{
					ErrorMessage: fmt.Sprintf("Category \"%s\" not found", id),
				},
			}
		} else {
			response = responses.PricesResponse{
				Response: responses.Response{
					ErrorMessage: fmt.Sprintf("Error while fetching suggested price for category \"%s\"", id),
				},
			}
		}
	} else {
		response = responses.PricesResponse{
			SuggestedPrice: suggestedPrice.SuggestedPrice,
			MinPrice:       suggestedPrice.MinPrice,
			MaxPrice:       suggestedPrice.MaxPrice,
		}
	}

	return response, err
}

// Calculates the minimum, maximum and suggested item price
// for a category of items. This method instantly returns
// and runs as a background process.
func (s *MELICategoriesService) UpdatePrice(id string) (responses.UpdatePricesResponse, error) {
	category, err := s.Products.Category(id)

	if err != nil {
		return responses.UpdatePricesResponse{
			Response: responses.Response{
				ErrorMessage: err.Error(),
			},
		}, err
	} else {
		return s.doUpdate([]models.Category{category})
	}
}

// Calculates the minimum, maximum and suggested item price
// for all categories of items. This method instantly returns
// and runs as a background process.
func (s *MELICategoriesService) UpdatePrices() (responses.UpdatePricesResponse, error) {
	// Reads all root categories and processes them.
	categories, err := s.Products.AllRootCategories()

	if err != nil {
		return responses.UpdatePricesResponse{
			Response: responses.Response{
				ErrorMessage: "Error getting all root categories",
			},
		}, err
	} else {
		return s.doUpdate(categories)
	}
}

// Receives a collection of categories, finds all leaf categories of these nodes
// and calculates the related prices for every one of them.
func (s *MELICategoriesService) doUpdate(categories []models.Category) (responses.UpdatePricesResponse, error) {
	response := responses.UpdatePricesResponse{}
	var err error = nil

	// This channel is readed by a number of gourutines, and it transfers found
	// leaf categories
	var leafCategories chan string = make(chan string)

	go func() {
		// In a separate goroutine, each category is travelled for finding its leafs,
		// and then written to the channel
		for _, cat := range categories {
			category, err := s.Products.Category(cat.ID)

			if err != nil {
				fmt.Printf("Error searching for category %s: %s\n", cat.ID, err)
			} else {
				s.walkCategoryTree(leafCategories, category)
			}
		}

		close(leafCategories)
	}()

	var waitGroup sync.WaitGroup
	waitGroup.Add(LEAF_CHANNEL_THREADS_COUNT)

	// Spawns a number of gourutines that will read the leaf categories
	// channel until it's closed.
	for i := 0; i < LEAF_CHANNEL_THREADS_COUNT; i++ {
		go func() {
			s.leafCategoriesReader(leafCategories)
			waitGroup.Done()
		}()
	}

	go func() {
		// Wait that all threads finish processing
		waitGroup.Wait()
		log.Println("**** Finished updating category prices ****")
	}()

	response.Message = "OK"
	return response, err
}

// Recursive traversal of a category node. When it finds a leaf category, it
// transfers it through the channel.
func (s *MELICategoriesService) walkCategoryTree(leafCategories chan<- string, category models.Category) {
	log.Println("Searching category " + category.ID)

	if category.IsLeaf() {
		log.Println("-------- Leaf category found:" + category.ID)
		leafCategories <- category.ID
	} else {
		for _, c := range category.ChildrenCategories {
			subCategory, err := s.Products.Category(c.ID)
			if err != nil {
				fmt.Printf("Error searching for leaf categories: %s\n", err)
			} else {
				s.walkCategoryTree(leafCategories, subCategory)
			}
		}
	}
}

// Reads the channel, processes the category for finding the prices, and saves
// them.
func (s *MELICategoriesService) leafCategoriesReader(leafCategories <-chan string) {
	for leafCategoryID := range leafCategories {
		log.Println("*** Updating category " + leafCategoryID)

		items, err := s.getMostRelevantItems(leafCategoryID)
		if err != nil {
			fmt.Printf("Error getting most relevant items: %s\n", err)
			continue
		}

		suggestedPrice := getSuggestedPrice(items, leafCategoryID)
		err = s.Repository.SaveOrUpdate(suggestedPrice)

		if err != nil {
			fmt.Printf("Error saving new suggested price: %s\n", err)
		} else {
			log.Println("****** Finished updating category " + leafCategoryID)
		}
	}
}

// Gets the most relevant products for a category. The criteria for finding these
// products is the next:
//
//   * Utilices MELI's internal algorithms for getting the product list sorted by best relevance.
//   * Only new products, not used ones.
//   * Only ready-to-buy products, no auctions.
//   * Bring only products published by the best sellers.
//
// Of all responses takes a finite sample that will be considered for analysing the prices.
func (s *MELICategoriesService) getMostRelevantItems(category string) ([]models.SearchItem, error) {
	var err error = nil

	items := []models.SearchItem{}
	offset := 0
	total := QUERY_MAX_ITEMS

	for offset < total {
		var searchResult models.SearchResult
		searchResult, err = s.Products.Search([]services_products.Argument{
			services_products.Argument{services_products.CATEGORY, category},
			services_products.Argument{services_products.OFFSET, strconv.Itoa(offset)},
			services_products.Argument{services_products.SORT, services_products.SORT_RELEVANCE},
			services_products.Argument{services_products.CONDITION, services_products.CONDITION_NEW},
			services_products.Argument{services_products.BUYING_MODE, services_products.BUYING_MODE_BUY_IT_NOW},
			services_products.Argument{services_products.ADULT_CONTENT, services_products.YES},
			services_products.Argument{services_products.POWER_SELLER, services_products.YES},
		})

		if err != nil {
			err = errors.New("Error querying items: " + err.Error())
			break
		} else {
			if searchResult.Paging.Total < QUERY_MAX_ITEMS {
				total = searchResult.Paging.Total
			}

			for _, r := range searchResult.Results {
				items = append(items, r)
			}

			offset += searchResult.Paging.Limit
		}
	}

	return items, err
}

// Of the products sample, obtains the suggested price. The statistical
// criterion used to calculate the price is the median (https://en.wikipedia.org/wiki/Median)
func getSuggestedPrice(items []models.SearchItem, categoryID string) models.SuggestedPrice {
	var suggestedPrice float32 = 0
	var minPrice float32 = 0
	var maxPrice float32 = 0

	// For calculating the median price, the list must be sorted.
	sort.Slice(items, func(i int, j int) bool {
		return items[i].Price < items[j].Price
	})

	if len(items) > 0 {
		if len(items)%2 == 0 {
			firstMiddle := items[(len(items)/2)-1]
			lastMiddle := items[len(items)/2]

			suggestedPrice = (firstMiddle.Price + lastMiddle.Price) / 2
		} else {
			suggestedPrice = items[len(items)/2].Price
		}

		minPrice = items[0].Price
		maxPrice = items[len(items)-1].Price
	}

	return models.SuggestedPrice{
		CategoryID:     categoryID,
		MinPrice:       minPrice,
		MaxPrice:       maxPrice,
		SuggestedPrice: suggestedPrice,
	}
}
