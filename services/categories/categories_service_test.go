package services

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"meli-exercise/models"
	"meli-exercise/models/responses"
	"meli-exercise/repositories"
	"reflect"
	"testing"
	"time"

	"meli-exercise/services/products"
)

const FAKE_CATEGORY string = "FAKE123"
const FAIL_CATEGORY string = "FAIL456"

const GUITAR_CATEGORY string = "MLA12497"

// This mock categories repository returns only one suggested price,
// or else a not found error
type MockCategoriesRepository struct{}

func (r *MockCategoriesRepository) Get(categoryID string) (models.SuggestedPrice, error) {
	if categoryID == FAKE_CATEGORY {
		return models.SuggestedPrice{
			CategoryID:     FAKE_CATEGORY,
			MaxPrice:       100.0,
			MinPrice:       50.0,
			SuggestedPrice: 60.0,
		}, nil
	} else {
		return models.SuggestedPrice{}, &repositories.ItemNotFoundError{ID: categoryID}
	}
}

func (r *MockCategoriesRepository) Close() {}

func (r *MockCategoriesRepository) Init() error {
	return nil
}

func (r *MockCategoriesRepository) SaveOrUpdate(suggestedPrice models.SuggestedPrice) error {
	return nil
}

// This mock categories repository always fails
type MockCategoriesFailureRepository struct{}

func (r *MockCategoriesFailureRepository) Get(categoryID string) (models.SuggestedPrice, error) {
	return models.SuggestedPrice{}, errors.New(fmt.Sprintf("Error while fetching \"%s\" category", categoryID))
}

func (r *MockCategoriesFailureRepository) Close() {}

func (r *MockCategoriesFailureRepository) Init() error {
	return nil
}

func (r *MockCategoriesFailureRepository) SaveOrUpdate(suggestedPrice models.SuggestedPrice) error {
	return errors.New(fmt.Sprintf("Error while saving \"%s\" category", suggestedPrice.CategoryID))
}

// This mock products service returns only one item
// and one category
type MockProductsService struct{}

func (p *MockProductsService) Init() {}

func (p *MockProductsService) Search(params []services_products.Argument) (models.SearchResult, error) {
	return models.SearchResult{Results: []models.SearchItem{models.SearchItem{
		Id:    "ITEM1",
		Price: 2100,
	}}}, nil
}

func (p *MockProductsService) Category(categoryID string) (models.Category, error) {
	return models.Category{ID: FAKE_CATEGORY}, nil
}

func (p *MockProductsService) AllRootCategories() ([]models.Category, error) {
	return []models.Category{models.Category{ID: FAKE_CATEGORY}}, nil
}

// This mock products service always returns empty responses
type MockProductsServiceEmpty struct{}

func (p *MockProductsServiceEmpty) Init() {}

func (p *MockProductsServiceEmpty) Search(params []services_products.Argument) (models.SearchResult, error) {
	return models.SearchResult{Results: []models.SearchItem{}}, nil
}

func (p *MockProductsServiceEmpty) Category(categoryID string) (models.Category, error) {
	return models.Category{}, nil
}

func (p *MockProductsServiceEmpty) AllRootCategories() ([]models.Category, error) {
	return []models.Category{}, nil
}

// This mock products service returns categories with the same ID
// that is provided
type MockProductsServiceChilds struct{}

func (p *MockProductsServiceChilds) Init() {}

func (p *MockProductsServiceChilds) Search(params []services_products.Argument) (models.SearchResult, error) {
	return models.SearchResult{Results: []models.SearchItem{}}, nil
}

func (p *MockProductsServiceChilds) Category(categoryID string) (models.Category, error) {
	return models.Category{
		ID: categoryID,
	}, nil
}

func (p *MockProductsServiceChilds) AllRootCategories() ([]models.Category, error) {
	return []models.Category{}, nil
}

// This mock products service always returns an error
// when trying to find a product
type MockProductsServiceFailureResponse struct{}

func (p *MockProductsServiceFailureResponse) Init() {}

func (p *MockProductsServiceFailureResponse) Search(params []services_products.Argument) (models.SearchResult, error) {
	return models.SearchResult{Results: []models.SearchItem{}}, errors.New("Error while finding products")
}

func (p *MockProductsServiceFailureResponse) Category(categoryID string) (models.Category, error) {
	return models.Category{}, nil
}

func (p *MockProductsServiceFailureResponse) AllRootCategories() ([]models.Category, error) {
	return []models.Category{}, nil
}

// A unit test for getting a saved category best price
func TestPrices(t *testing.T) {
	service := MELICategoriesService{
		Repository: &MockCategoriesRepository{},
	}

	expectedPriceResponse := responses.PricesResponse{
		MaxPrice:       100.0,
		MinPrice:       50.0,
		SuggestedPrice: 60.0,
	}

	price, err := service.Prices(FAKE_CATEGORY)

	if err != nil {
		t.Fatalf("There was an error in TestPrices: %s", err)
	}

	if price != expectedPriceResponse {
		t.Fatalf("Expected %+v but got %+v", expectedPriceResponse, price)
	}
}

// A unit test for checking a "not found" error
// while getting a saved category best price
func TestPricesNotFound(t *testing.T) {
	service := MELICategoriesService{
		Repository: &MockCategoriesRepository{},
	}

	price, err := service.Prices(FAIL_CATEGORY)

	if err == nil {
		t.Fatal("Expected an error in TestPrices, but there's none")
	}

	if price.Response.ErrorMessage == "" {
		t.Fatal("Expected an error message in TestPrices, but it's empty")
	}
}

// A unit test for checking an error while getting
// a saved category best price
func TestPricesFailure(t *testing.T) {
	service := MELICategoriesService{
		Repository: &MockCategoriesFailureRepository{},
	}

	price, err := service.Prices(FAIL_CATEGORY)

	if err == nil {
		t.Fatal("Expected an error in TestPrices, but there's none")
	}

	if price.Response.ErrorMessage == "" {
		t.Fatal("Expected an error message in TestPrices, but it's empty")
	}
}

// A unit test for finding a leaf category
// in a tree that has only one node
func TestWalkCategoryTreeLeafCategory(t *testing.T) {
	service := MELICategoriesService{}

	leafCategories := make(chan string)
	defer close(leafCategories)

	go func() {
		service.walkCategoryTree(leafCategories, models.Category{ID: FAKE_CATEGORY})
	}()

	expected := FAKE_CATEGORY
	found := <-leafCategories

	if found != expected {
		t.Errorf("Expected %s but got %s", expected, found)
	}
}

// A unit test for finding leafs categories
// in a tree with root and child nodes
func TestWalkCategoryTree(t *testing.T) {
	service := MELICategoriesService{
		Products: &MockProductsServiceChilds{},
	}

	leafCategories := make(chan string)
	defer close(leafCategories)

	go func() {
		service.walkCategoryTree(leafCategories, models.Category{
			ID: FAKE_CATEGORY + "_0",
			ChildrenCategories: []models.Category{
				models.Category{ID: FAKE_CATEGORY + "_1"},
				models.Category{ID: FAKE_CATEGORY + "_2"},
			},
		})
	}()

	expectedCategoryIDs := []string{FAKE_CATEGORY + "_1", FAKE_CATEGORY + "_2"}
	foundCategoryIDs := []string{}

	for i := 0; i < len(expectedCategoryIDs); i++ {
		foundCategoryIDs = append(foundCategoryIDs, <-leafCategories)
	}

	if !reflect.DeepEqual(expectedCategoryIDs, foundCategoryIDs) {
		t.Errorf("Expected %s but got %s", expectedCategoryIDs, foundCategoryIDs)
	}
}

// A unit test for getting only relevant items
// for a category
func TestGetMostRelevantItems(t *testing.T) {
	service := MELICategoriesService{
		Products: &MockProductsService{},
	}

	expectedItems := []models.SearchItem{models.SearchItem{
		Id:    "ITEM1",
		Price: 2100,
	}}

	items, err := service.getMostRelevantItems(FAKE_CATEGORY)

	if err != nil {
		t.Fatalf("There was an error in TestGetMostRelevantItems: %s", err)
	}

	if !reflect.DeepEqual(items, expectedItems) {
		t.Fatalf("Expected %+v but got %+v", expectedItems, items)
	}
}

// A unit test for testing when a category has no items
func TestGetMostRelevantItemsEmptyResponse(t *testing.T) {
	service := MELICategoriesService{
		Products: &MockProductsServiceEmpty{},
	}

	expectedEmptyItems := []models.SearchItem{}

	items, err := service.getMostRelevantItems(FAKE_CATEGORY)

	if err != nil {
		t.Fatalf("There was an error in TestGetMostRelevantItemsEmptyResponse: %s", err)
	}

	if !reflect.DeepEqual(items, expectedEmptyItems) {
		t.Fatalf("Expected %+v but got %+v", expectedEmptyItems, items)
	}
}

// A unit test for testing when there's an error
// searching items for a category
func TestGetMostRelevantItemsFailureResponse(t *testing.T) {
	service := MELICategoriesService{
		Products: &MockProductsServiceFailureResponse{},
	}

	expectedEmptyItems := []models.SearchItem{}

	items, err := service.getMostRelevantItems(FAKE_CATEGORY)

	if err == nil {
		t.Fatal("Expected an error in TestGetMostRelevantItemsFailureResponse, but there's none")
	}

	if !reflect.DeepEqual(items, expectedEmptyItems) {
		t.Fatalf("Expected %+v but got %+v", expectedEmptyItems, items)
	}
}

// A unit test for getting the suggested price ($0)
// when a category has no items
func TestGetSuggestedPriceEmptyItems(t *testing.T) {
	items := []models.SearchItem{}

	expectedSuggestedPriceEmpty := models.SuggestedPrice{
		CategoryID: FAKE_CATEGORY,
	}

	suggestedPrice := getSuggestedPrice(items, FAKE_CATEGORY)
	if suggestedPrice != expectedSuggestedPriceEmpty {
		t.Fatal("Expected an empty suggested price")
	}
}

// A unit test for getting the suggested price for a category
// when the number of items is even
func TestGetSuggestedPriceEvenItems(t *testing.T) {
	items := []models.SearchItem{
		models.SearchItem{Price: 3},
		models.SearchItem{Price: 10},
		models.SearchItem{Price: 36},
		models.SearchItem{Price: 255},
		models.SearchItem{Price: 79},
		models.SearchItem{Price: 24},
		models.SearchItem{Price: 5},
		models.SearchItem{Price: 8},
	}

	expectedSuggestedPriceEven := models.SuggestedPrice{
		CategoryID: FAKE_CATEGORY, MinPrice: 3, MaxPrice: 255,
		SuggestedPrice: 17,
	}

	suggestedPrice := getSuggestedPrice(items, FAKE_CATEGORY)

	if suggestedPrice != expectedSuggestedPriceEven {
		t.Fatalf("Expected %+v but got %+v", expectedSuggestedPriceEven, suggestedPrice)
	}
}

// A unit test for getting the suggested price for a category
// when the number of items is odd
func TestGetSuggestedPriceOddItems(t *testing.T) {
	items := []models.SearchItem{
		models.SearchItem{Price: 2},
		models.SearchItem{Price: 8},
		models.SearchItem{Price: 30},
		models.SearchItem{Price: 5},
		models.SearchItem{Price: 16},
		models.SearchItem{Price: 11},
		models.SearchItem{Price: 21},
	}

	expectedSuggestedPriceOdd := models.SuggestedPrice{
		CategoryID: FAKE_CATEGORY, MinPrice: 2, MaxPrice: 30,
		SuggestedPrice: 11,
	}

	suggestedPrice := getSuggestedPrice(items, FAKE_CATEGORY)
	if suggestedPrice != expectedSuggestedPriceOdd {
		t.Fatalf("Expected %+v but got %+v", expectedSuggestedPriceOdd, suggestedPrice)
	}
}

// A unit test for testing the channel that
// reads categories and then processes them
func TestLeafCategoriesReader(t *testing.T) {
	service := MELICategoriesService{
		Products:   &MockProductsService{},
		Repository: &MockCategoriesRepository{},
	}

	leafCategories := make(chan string)
	defer close(leafCategories)

	go func() {
		service.leafCategoriesReader(leafCategories)
	}()

	leafCategories <- FAKE_CATEGORY
}

// A big test for checking the complete flow of
// the UpdatePrice method, but with mocked data
func TestUpdatePrice(t *testing.T) {
	service := MELICategoriesService{
		Products:   &MockProductsService{},
		Repository: &MockCategoriesRepository{},
	}

	response, err := service.UpdatePrice(FAKE_CATEGORY)
	if err != nil {
		t.Fatalf("There was an error in TestUpdatePrice: %s", err)
	}

	expectedResponse := responses.UpdatePricesResponse{Message: "OK"}
	if response != expectedResponse {
		t.Fatalf("Expected %+v but got %+v", response, expectedResponse)
	}
}

// A big test for checking the complete flow of
// the UpdatePrices method, but with mocked data
func TestUpdatePrices(t *testing.T) {
	service := MELICategoriesService{
		Products:   &MockProductsService{},
		Repository: &MockCategoriesRepository{},
	}

	response, err := service.UpdatePrices()
	if err != nil {
		t.Fatalf("There was an error in TestUpdatePrice: %s", err)
	}

	expectedResponse := responses.UpdatePricesResponse{Message: "OK"}
	if response != expectedResponse {
		t.Fatalf("Expected %+v but got %+v", response, expectedResponse)
	}
}

// Benchmark for GetSuggestedPrice, one item
func BenchmarkGetSuggestedPrice1(b *testing.B) { benchmarkGetSuggestedPrice(1, b) }

// Benchmark for GetSuggestedPrice, 100 items
func BenchmarkGetSuggestedPrice100(b *testing.B) { benchmarkGetSuggestedPrice(100, b) }

// Benchmark for GetSuggestedPrice, 10000 items
func BenchmarkGetSuggestedPrice10000(b *testing.B) { benchmarkGetSuggestedPrice(10000, b) }

// Benchmark for GetSuggestedPrice, 100000 items
func BenchmarkGetSuggestedPrice100000(b *testing.B) { benchmarkGetSuggestedPrice(100000, b) }

func buildItems(count int) []models.SearchItem {
	items := []models.SearchItem{}

	// Feed with random data
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < count; i++ {
		p := rand.Float32() * 1000
		items = append(items, models.SearchItem{Price: p})
	}

	return items
}

func benchmarkGetSuggestedPrice(count int, b *testing.B) {
	items := buildItems(count)

	for n := 0; n < b.N; n++ {
		getSuggestedPrice(items, FAKE_CATEGORY)
	}
}

// Benchmark for Prices method
func BenchmarkPrices(b *testing.B) {
	repository := repositories.MELICategoriesRepository{}
	err := repository.Init()
	defer repository.Close()

	if err != nil {
		log.Fatal("Could not establish communication to categories repository. Error: ", err)
	}

	service := &MELICategoriesService{
		Repository: &repository,
	}

	for n := 0; n < b.N; n++ {
		service.Prices(GUITAR_CATEGORY)
	}
}

// Benchmark for Prices method, but with mocked data
func BenchmarkPricesMockData(b *testing.B) {
	service := &MELICategoriesService{
		Repository: &MockCategoriesRepository{},
	}

	for n := 0; n < b.N; n++ {
		service.Prices(FAKE_CATEGORY)
	}
}

// Benchmark for UpdatePrice method
func BenchmarkUpdatePrice(b *testing.B) {
	products := services_products.MeliProductsService{}
	products.Init()

	repository := repositories.MELICategoriesRepository{}
	err := repository.Init()
	defer repository.Close()

	if err != nil {
		log.Fatal("Could not establish communication to categories repository. Error: ", err)
	}

	service := &MELICategoriesService{
		Repository: &repository,
		Products:   &products,
	}

	for n := 0; n < b.N; n++ {
		service.UpdatePrice(GUITAR_CATEGORY)
	}
}
