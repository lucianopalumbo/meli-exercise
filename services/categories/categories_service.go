package services

import (
	"meli-exercise/models/responses"
)

// A service oriented to operate with product categories.
type CategoriesService interface {
	// Obtains the minimum, maximum and suggested item price
	// for a category of items.
	Prices(id string) (responses.PricesResponse, error)

	// Calculates the minimum, maximum and suggested item price
	// for a category of items. This method instantly returns
	// and runs as a background process.
	UpdatePrice(id string) (responses.UpdatePricesResponse, error)

	// Calculates the minimum, maximum and suggested item price
	// for all categories of items. This method instantly returns
	// and runs as a background process.
	UpdatePrices() (responses.UpdatePricesResponse, error)
}
