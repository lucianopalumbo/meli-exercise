![Logo](docs/assets/logo.png)

# Meli-Price-Suggester
*****


In MercadoLibre everyday thousands of users make millions of publications, and one of the
main challenges that they face when they are creating them is to understand which could be the ideal
price for its product. A high price could be uncompetitive, and a low price reduces earnings.

**Meli-Price-Suggester** is an API for helping users to choose the ideal price for its product.


## Prerequisites

To build the project, you will need:

- Go 1.10.222 - [Go Programming Language](https://golang.org/dl/)
- dep - [Dependency management for Go](https://golang.github.io/dep/docs/installation.html)

## Getting Started

Clone the repository and download the project in your local workspace.

```
$ git clone https://bitbucket.org/lucianopalumbo/meli-exercise.git
$ cd meli-exercise
```

## Building

Clean the working directory, ensure that all dependencies have been downloaded,
and then build the binary.

```
$ go clean
$ dep ensure
$ go build
```

## Running the application

This application uses [viper](https://github.com/spf13/viper) for managing configuration properties. It means that it can read properties from system environment variables, or from a configuration file.
You can create a file in the root of the project named **config.json**, with the next
properties:

```
{
  "DATABASE_ADDRESSES" : "cluster1:27017,cluster2:27017,cluster3:27017",
  "DATABASE_AUTH_NAME": "auth",
  "DATABASE_USERNAME" : "username",
  "DATABASE_PASSWORD": "here_goes_a_secret"
}
```

All keys are related to the configuration of a MongoDB server in which the application
stores the prices of the products:

 * `DATABASE_ADDRESSES` : Comma separated values with database cluster endpoints
 * `DATABASE_AUTH_NAME` : Database name which is used for authentication
 * `DATABASE_USERNAME` : User name
 * `DATABASE_PASSWORD` : Password

You can also set these values in environment variables, for example:

```
$ export DATABASE_PASSWORD=here_goes_a_secret
```

Or passing them directly in the command line:

```
$ DATABASE_PASSWORD=here_goes_a_secret ./meli-exercise
```

In any case, for running the application you must run the compiled binary:

```
$ ./meli-exercise
```

## Tests

This application has a series of unit and integration tests for checking correct
functionality.

### Unit Test

For running only unit tests, in the project's root directory run:

```
$ go test ./...
```

### Integration Tests

In the case of integration tests, it's necessary to specify a build tag for running them:

```
$ go test ./... -tags dbintegration
```

### Coverage

This command runs unit and integration tests, and then it generates coverage report
in a separate file:

```
$ go test ./... -tags dbintegration -coverprofile <output_file>

```

For reading the coverage report, use:

```
$ go tool cover -func <output_file>
```

### Benchmarks

This application also has benchmark checks. For running them, use:

```
$ go test -bench=.
```

## Usage

Basically, there are three endpoints exposed:

* Get the suggested price for a category of products:
```
$ curl -GET http://<endpoint>/categories/<category_id>/prices/
```

 * Example:

  ```
  $ curl -GET http://localhost:8080/categories/MLA3530/prices/
  ```

* Updating and saving the suggested price for a category of products:
```
$ curl -PUT http://<endpoint>/categories/<category_id>/prices
```
 * Example:

 ```
 $ curl -PUT http://localhost:8080/categories/MLA86659/prices
 ```

* Updating and saving the suggested price for all categories:
```
$ curl -POST http://<endpoint>/categories/prices/
```
 * Example:

 ```
 $ curl -POST http://localhost:8080/categories/prices/
 ```
