package main

import (
	"log"
	"meli-exercise/config"
	"meli-exercise/controllers"
	"meli-exercise/repositories"
	"meli-exercise/services/categories"
	"meli-exercise/services/products"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

// Starting point of the program.
func main() {
	v, _ := ReadConfiguration("config")
	err := config.AppConfiguration.Init(v)

	if err != nil {
		log.Fatal("There was an error reading app configuration. Error: \n", err)
	}

	controller, repository := SetupDependencies()
	defer repository.Close()

	router := SetupRouter(controller)
	router.Run(":8080")
}

// Maps HTTP endpoints to controller methods.
func SetupRouter(controller controllers.CategoriesController) *gin.Engine {
	router := gin.Default()

	categories := router.Group("/categories")
	{
		categories.POST("/prices", controller.UpdatePrices)
		categories.PUT("/:id/prices", controller.UpdatePrice)
		categories.GET("/:id/prices", controller.Prices)
	}

	return router
}

// Initializes the program controllers, services, and repositories.
func SetupDependencies() (controllers.CategoriesController, repositories.CategoriesRepository) {
	// Dependencies are injected into the controller and the services
	repository := repositories.MELICategoriesRepository{}
	err := repository.Init()

	if err != nil {
		log.Fatal("Could not establish communication to categories repository. Error: ", err)
	}

	products := services_products.MeliProductsService{}
	products.Init()

	controller := controllers.CategoriesController{
		Service: &services.MELICategoriesService{
			Repository: &repository,
			Products:   &products,
		},
	}

	return controller, &repository
}

func ReadConfiguration(fileName string) (*viper.Viper, error) {
	v := viper.New()

	v.SetConfigName(fileName)
	v.AddConfigPath(".")
	v.AutomaticEnv()

	err := v.ReadInConfig()
	return v, err
}
