package repositories

import (
	"fmt"
	"meli-exercise/models"
)

// Repository for storing information about categories.
type CategoriesRepository interface {
	// Initializes the repository. Must be called first before using it.
	Init() error

	// Gets a category suggested price by its ID.
	Get(categoryID string) (models.SuggestedPrice, error)

	// Updates a suggested price, or saves it if it doesn't exists.
	SaveOrUpdate(suggestedPrice models.SuggestedPrice) error

	// Closes the connection to the repository.
	Close()
}

// Error that is thrown when an item is not found.
type ItemNotFoundError struct {
	ID string
}

// Gets the error message as a string.
func (e *ItemNotFoundError) Error() string {
	return fmt.Sprintf("%s not found", e.ID)
}
