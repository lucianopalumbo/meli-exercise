package repositories

import (
	"crypto/tls"
	"net"

	"meli-exercise/config"
	"meli-exercise/models"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Implementation of a NoSQL database for storing categories.
type MELICategoriesRepository struct{}

const NOT_FOUND string = "not found"

// This object is used to communicate with MongoDB.
var mainSession *mgo.Session

// Initializes the repository. Must be called first before using it.
func (r *MELICategoriesRepository) Init() error {
	var err error

	tlsConfig := &tls.Config{}

	dialInfo := &mgo.DialInfo{
		Addrs:    config.AppConfiguration.DatabaseAddresses,
		Database: config.AppConfiguration.Database,
		Username: config.AppConfiguration.Username,
		Password: config.AppConfiguration.Password,
	}

	dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}
	mainSession, err = mgo.DialWithInfo(dialInfo)

	return err
}

// Closes the connection to the repository.
func (r *MELICategoriesRepository) Close() {
	mainSession.Close()
}

// Gets a category suggested price by its ID.
func (r *MELICategoriesRepository) Get(categoryID string) (models.SuggestedPrice, error) {
	session := mainSession.Copy()
	defer session.Close()

	var suggestedPrice models.SuggestedPrice
	c := session.DB("test").C("suggested_prices")
	err := c.Find(bson.M{"categoryid": categoryID}).One(&suggestedPrice)

	if err != nil && err.Error() == NOT_FOUND {
		err = &ItemNotFoundError{ID: categoryID}
	}

	return suggestedPrice, err
}

// Updates a suggested price, or saves it if it doesn't exists.
func (r *MELICategoriesRepository) SaveOrUpdate(suggestedPrice models.SuggestedPrice) error {
	session := mainSession.Copy()
	defer session.Close()

	c := session.DB("test").C("suggested_prices")
	_, err := c.Upsert(bson.M{"categoryid": suggestedPrice.CategoryID}, suggestedPrice)

	return err
}
