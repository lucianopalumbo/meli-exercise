package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

const DATABASE_ADDRESSES string = "DATABASE_ADDRESSES"
const DATABASE_AUTH_NAME string = "DATABASE_AUTH_NAME"
const DATABASE_USERNAME string = "DATABASE_USERNAME"
const DATABASE_PASSWORD string = "DATABASE_PASSWORD"

// The app configuration.
var AppConfiguration Config

type Config struct {
	// Database server.
	DatabaseAddresses []string

	// Database auth name.
	Database string

	// Database username.
	Username string

	// Secret.
	Password string
}

// Error that is thrown when the app fails to load its configuration.
type ConfigError struct {
	Errors []string
}

// Gets the error message as a string.
func (e *ConfigError) Error() string {
	return strings.Join(e.Errors, "\n")
}

// Initializes app configuration. Can read from a file named "config.json" in
// app's root directory, or from environment variables.
func (c *Config) Init(v *viper.Viper) error {
	var err error = nil
	errors := []string{}

	addresses := v.GetString(DATABASE_ADDRESSES)
	if addresses == "" {
		errors = append(errors, fmt.Sprintf("Field %s is empty", DATABASE_ADDRESSES))
	}

	AppConfiguration = Config{
		DatabaseAddresses: strings.Split(addresses, ","),
		Database:          v.GetString(DATABASE_AUTH_NAME),
		Username:          v.GetString(DATABASE_USERNAME),
		Password:          v.GetString(DATABASE_PASSWORD),
	}

	if AppConfiguration.Database == "" {
		errors = append(errors, fmt.Sprintf("Field %s is empty", DATABASE_AUTH_NAME))
	}

	if AppConfiguration.Username == "" {
		errors = append(errors, fmt.Sprintf("Field %s is empty", DATABASE_USERNAME))
	}

	if AppConfiguration.Password == "" {
		errors = append(errors, fmt.Sprintf("Field %s is empty", DATABASE_PASSWORD))
	}

	if len(errors) > 0 {
		err = &ConfigError{Errors: errors}
	}

	return err
}
