// +build dbintegration

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"meli-exercise/config"
	"meli-exercise/models/responses"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

const CATEGORY string = "MLA86659"

// An integration test for getting the suggested price of a category.
func TestPrices(t *testing.T) {
	gin.SetMode(gin.TestMode)

	v, _ := ReadConfiguration("config")
	err := config.AppConfiguration.Init(v)

	if err != nil {
		log.Fatal("There was an error reading app configuration. Error: \n", err)
	}

	controller, repository := SetupDependencies()
	defer repository.Close()

	testRouter := SetupRouter(controller)

	// Makes an HTTP request to the "prices" endpoint
	req, err := http.NewRequest("GET", fmt.Sprintf("/categories/%s/prices", CATEGORY), nil)
	req.Header.Set("Content-Type", "application/json")

	if err != nil {
		t.Fatalf("Error creating request: %d.", err)
	}

	resp := httptest.NewRecorder()
	testRouter.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Fatalf("/categories/%s/prices failed with error code %d.", CATEGORY, resp.Code)
	}
}

// An integration test for obtaining and saving the suggested price of a category.
func TestUpdatePrice(t *testing.T) {
	gin.SetMode(gin.TestMode)

	v, _ := ReadConfiguration("config")
	err := config.AppConfiguration.Init(v)

	if err != nil {
		log.Fatal("There was an error reading app configuration. Error: \n", err)
	}

	controller, repository := SetupDependencies()
	defer repository.Close()

	testRouter := SetupRouter(controller)

	req, err := http.NewRequest("PUT", fmt.Sprintf("/categories/%s/prices", CATEGORY), nil)
	req.Header.Set("Content-Type", "application/json")

	if err != nil {
		t.Fatalf("Error creating request: %d.", err)
	}

	resp := httptest.NewRecorder()
	testRouter.ServeHTTP(resp, req)

	if resp.Code != http.StatusOK {
		t.Fatalf("/categories/%s/prices failed with error code %d.", CATEGORY, resp.Code)
	}

	// Checks if response is OK
	expectedResponse := responses.UpdatePricesResponse{
		Message: "OK",
	}

	var response responses.UpdatePricesResponse
	json.Unmarshal(resp.Body.Bytes(), &response)

	if response != expectedResponse {
		t.Fatalf("Expected %+v but got %+v", expectedResponse, response)
	}
}
